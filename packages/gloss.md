---
tags: [package, gloss]
---

# Gloss

- Hackage: <https://hackage.haskell.org/package/gloss>
- Source: <https://github.com/benl23x5/gloss>

> Gloss hides the pain of drawing simple vector graphics behind a nice data type and a few display functions. Gloss uses OpenGL under the hood, but you won't need to worry about any of that. Get something cool on the screen in under 10 minutes.

## Minimal example

```haskell
-- | Display "Hello World" in a window.
import Graphics.Gloss

main :: IO ()
main =
  display
    (InWindow
      "Hello World" -- window title
      (400, 150)    -- window size
      (10, 10)      -- window position
    )
  white   -- background color
  picture -- picture to display

picture :: Picture
picture =
  -- shift the text to the middle of the window
  Translate (-170) (-20) $
    -- display it half the original size
    Scale 0.5 0.5 $
      -- the shape is a text
      Text "Hello World"
```
