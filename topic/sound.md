---
tags: [topic, sound]
---

# Sound

Adding sounds is a great way to boost "completeness" feeling of a game.
And it isn't that hard!
You will have to hear through a lot of chaff, but it is possible to find quality SFX to match your environment.
Youtube has freebie royalty-free music that you can download.

## SDL Mixer

Offers straight-forward flow and a variety of formats to load via 3rd party libraries.

- Initialize Audio subsystem of SDL.
- Initialize Mixer state.
- Load resources.
- Set background `Music` or play `Chunks` whenever it is appropriate, presumably near your event loop.

## OpenAL

Gives you OpenGL-flavored 3D positional audio and ability to play Haskell-generated buffers.

- Open Device.
- Create Context and make it current.
- Load resources into Buffers.
- Create static sources for music, UI etc. and attach Buffers to them.
- Create dynamic sources and keep them synced to world/absoulute or relative positions.

## MIDI

Not quite useful for games, but comes handy for livecoding and other performances.
Can be very powerful when combined with professional sound synthesis software.

Cross-platform compatibility is less of an issue, but [PortMIDI] (and its [PortMIDI-simple] wrapper) got your back if that is what you need.

Linux-only users can directly for [alsa-seq] directly.

MIDI is not only a sound IO protocol, but also a controller/interface protocol.
While simple, it can add multi-dimensional parameter variation to your code, even if completely unrelated to multimedia.

[alsa-seq]: https://hackage.haskell.org/package/alsa-seq
[PortMIDI]: https://hackage.haskell.org/package/PortMidi
[PortMIDI-simple]: https://hackage.haskell.org/package/PortMidi-simple
