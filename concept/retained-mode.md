---
tags: [concept]
---

# Retained Mode

Originially a Direct2D term, since diluted into "keep all the state on your GPU".
Contrast with: [[concept/immediate-mode|immediate mode]].

It is possible to load everything upfront and only use "get drawing" call of your API.
For most applications, however, it turns into long fat-tailed distribution of how much stuff do you want to update and how often.

There's a natural range of update intensity:

- Loaded at start and not changing ever: fonts, UI elements, models for simple games, camera projection (unless the window is resizeable).
- Loaded when needed, but then sit in memory until you get far away from that house/planet/level.
- Changes quite frequently, but not *every* frame: camera view and some model transforms.
- Does not make sense to retain for more than one draw call: particles, animations and effects, time.

OpenGL has buffer usage hints to reflect update patterns. Alas, they are only hints:

> Is it better to use STATIC for buffers that are updated very infrequently?
> Is it better to use DYNAMIC for buffers that get updated frequently, but not at STREAM speed?
> Is it better to use DYNAMIC for buffers that get partially updated?
> These are questions that can only be answered with careful profiling.
> And even then, the answer will only be accurate for that particular driver version from that particular hardware vendor.

One can go even further with indirect drawing buffers, where a buffer hold a batch of arguments for draw calls.

## Known users

- You, when doing things right.
