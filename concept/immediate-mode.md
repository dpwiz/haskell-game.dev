---
tags: [concept]
---

# Immediate Mode

Originially a Direct2D term, since diluted into "every frame start drawing from scratch".
Contrast with: [[concept/retained-mode|retained mode]].

Historically, OpenGL used function calls to emit vertices that taxed CPU heavily.
After a round of optimizations it got command lists and vertex buffers.
Later, the `glBegin`/`glEnd` and related functions got wiped from the Core Profile.
You can still get them with the Compatibility Profile, but not all devices support it.

## The good parts

- Being stateless allows to quickly turn your data into something on a screen.
- No need to track resources. Stop submitting commands and it's all gone.

## The bad parts

- You should keep your primitive usage down to a relatively low number.
- You only get some basic lighting models.

## The okay part

- You can gain more performance by using canned draw calls and buffers.
- This *can* be enough to make a real, playable and fun game.

## Somewhat related

### Immediate mode GUI

While not being "immediate mode" literally...

> A common misunderstanding is to mistake immediate mode gui for immediate mode rendering, which
> usually implies hammering your driver/GPU with a bunch of inefficient draw calls and state
> changes as the gui functions are called.
> This is NOT what Dear ImGui does.
> Dear ImGui outputs vertex buffers and a small list of draw calls batches.

... still kinda has that stateless aspect:

> An IMGUI tries to minimize superfluous state duplication, state synchronization and state
> retention from the user's point of view.

## Known users

- [[packages/gloss]]
- lots of triangle examples out there on the internet
