---
tags: [game, 2d, gl, glfw, linux, windows, multiplayer]
---

# Pixelpusher

> Arcade-style 2D multiplayer team combat game with simple mechanics.
> Jump into combat with direct control of a swarm of drones and their overseer, juggling attacking and dodging.
> Capture the flag with up to 32 players.

- Steam: <https://store.steampowered.com/app/2495130/Pixelpusher>
- Itch: <https://aetup.itch.io/pixelpusher>
- Source: <https://gitlab.com/awjchen/pixelpusher>

![2023-09-01](https://img.itch.zone/aW1hZ2UvODE3Nzg0LzEzNDIzNTkzLnBuZw==/original/vKGhWo.png)
