---
tags: [game, 2d, sdl, linux, windows]
---

# Nyx

> Nyx is a short bullet-hell/shoot-em-up game set in the 90's featuring retrowave themes.

- Site: https://gilmi.me/nyx
- Blog: https://gilmi.me/blog/post/2018/07/24/pfgames
- Source: https://gitlab.com/gilmi/nyx-game

![2020-12-04](https://gilmi.me/static/misc/nyx/nyx-gameplay.gif)
