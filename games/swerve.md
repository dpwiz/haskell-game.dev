---
tags: [game, 3d, vulkan, openal, linux, windows, gamejam, apecs]
---

# Swerve

> Stable orbits are too boring for a G-force seeking space racer.
> Test your skills and reaction time in this twisted mess of warp.

- Itch: <https://icrbow.itch.io/swerve>
  + Retrospect: <https://icrbow.itch.io/swerve/devlog/303644/just-one-more-thing>
- Source: <https://gitlab.com/dpwiz/swerve>
- Jam: <https://ldjam.com/events/ludum-dare/49/swerve>

![2021-12-20](https://img.itch.zone/aW1hZ2UvMTIyMjY3NC83MTM3MDk0LnBuZw==/original/yssAs7.png)
