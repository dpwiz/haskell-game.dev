---
tags: [game, 3d, vulkan, openal, linux, windows, gamejam, apecs]
---

# OrboROS

- Itch: <https://icrbow.itch.io/orboros>
  + Retrospect: <https://icrbow.itch.io/orboros/devlog/189752/ludum-dare-47-retrospect>
- Source: <https://gitlab.com/dpwiz/orboros>
- Jam: <https://ldjam.com/events/ludum-dare/47/orboros>

![2021-12-20](https://img.itch.zone/aW1hZ2UvNzc4Nzc1LzY5MTMwMDEuanBn/original/TroEcj.jpg)
