---
tags: [game, 2d, lambdahack, linux, windows, gamejam]
---

# Allure of the Stars

> Near-future Sci-Fi roguelike and tactical squad combat game.

- Site: <http://www.allureofthestars.com/>
- Itch: <https://man-of-letters.itch.io/allureofthestars>
- Source: <https://github.com/AllureOfTheStars/Allure/>

![2020-11-29](https://img.itch.zone/aW1nLzEyNzEwNDUuZ2lm/original/eeEr2%2B.gif)
