---
tags: [game, 2d, brick, osx, linux]
---

# Swarm

> Swarm is a 2D programming and resource gathering game.
> Program your robots to explore the world and collect resources, which in turn allows you to build upgraded robots that can run more interesting and complex programs.

- Website: <https://swarm-game.github.io/>
- Source: <https://github.com/byorgey/swarm>
- Announcement: <https://byorgey.wordpress.com/2021/09/23/swarm-preview-and-call-for-collaboration/>

![2021-12-20](https://swarm-game.github.io/images/tutorial/log.png)
