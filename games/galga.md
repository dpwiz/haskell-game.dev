---
tags: [game, 2d, elm, browser]
---

# GALGA

> Multiplayer card game written in Haskell and Elm.

- Site: <https://www.galgagame.com/>
- Blog: <https://roganmurley.com/2021/12/11/free-monads.html>
- Source: <https://github.com/RoganMurley/GALGAGAME>

![2021-12-20](https://roganmurley.com/assets/galga2.gif)
