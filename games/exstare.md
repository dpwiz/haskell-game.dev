---
tags: [game, 2d, apecs, opengl, linux, windows, gamejam]
---

# Exstare

- Itch: <https://icrbow.itch.io/exstare>
- Source: <https://gitlab.com/dpwiz/exstare>
- Jam: <https://ldjam.com/events/ludum-dare/46/exstare>

![2020-11-29](https://img.itch.zone/aW1hZ2UvNjE0NTE2LzMyOTA3MzYucG5n/original/TEQ90y.png)
