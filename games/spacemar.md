---
tags: [game, 2d, apecs, gloss, linux, windows, gamejam]
---

# SpaceMar!

> N-body gravity trading.

- Itch: <https://icrbow.itch.io/spacemar>
  + DevBlog: <https://icrbow.itch.io/spacemar/devlog>
- Source: <https://gitlab.com/dpwiz/spacemar>
- Jam: <https://itch.io/jam/games-made-quick-four/rate/545829>

![2020-11-29](https://img.itch.zone/aW1hZ2UvNTQ1ODI5LzMwMTA3MDgucG5n/original/9SdJTs.png)
