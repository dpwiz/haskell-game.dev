# Games

This site lists some of a recent, finished and playable games.

```query
path: games/*
```

> Know something great is amiss?
>
> [File it in](https://gitlab.com/dpwiz/haskell-game.dev/-/issues/new?issuable_template=game)!

Additionally, there were some games released on Hackage, listed under the [Game] category.

[Game]: https://hackage.haskell.org/packages/#cat:Game
