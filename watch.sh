#!/bin/bash
docker run --rm --tmpfs /tmp -p 8321:8321 -e LANG=C.UTF-8 -e LC_ALL=C.UTF-8 -v $PWD:/data sridca/emanote emanote -L "/data" run --host 0.0.0.0 --port 8321
